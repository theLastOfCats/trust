#!/usr/bin/env bash

xvfb-run sh -c "\
    wine py -m pip install --no-warn-script-location -r requirements.txt  && \
    wine ${WINEPREFIX}/drive_c/Python36-32/Scripts/pyinstaller --noupx --onefile trust/trust.py"