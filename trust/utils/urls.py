import ipaddress
from urllib.parse import urlparse


def is_url(entity: str) -> bool:
    try:
        ipaddress.ip_address(entity)
        return False
    except ValueError:
        return True


def is_ipaddress(entity: str) -> bool:
    try:
        ipaddress.ip_address(entity)
        return True
    except ValueError:
        return False


def get_domains_from_urls(url: str) -> str:
    parsed_url = urlparse(url)

    if parsed_url.netloc == '':
        if url.startswith('www'):
            url = '.'.join(url.split('.')[1:])
        url = '//' + url

    return urlparse(url).netloc


def get_domains_list(entities: set) -> set:
    return set(
        map(
            get_domains_from_urls,
            filter(is_url, entities)
        )
    )


def get_ipaddress_list(entities: set) -> set:
    return set(filter(is_ipaddress, entities))


def cleanup_entities(entities: set) -> set:
    return get_domains_list(entities).union(get_ipaddress_list(entities))
