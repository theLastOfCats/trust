import os
import shutil
import tempfile


# workaround for logging
def configure_log_dir():
    logdir = os.path.join(tempfile.gettempdir(), 'trust')
    if os.path.exists(logdir):
        shutil.rmtree(logdir)
    os.makedirs(logdir)


def get_logdir():
    return os.path.join(tempfile.gettempdir(), 'trust')
