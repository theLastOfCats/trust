import argparse
import logging
import sys

from trust.handlers.safe_browsing import SBL
from trust.handlers.sucuri import Sucuri
from trust.handlers.urlvoid import UrlVoid
from trust.handlers.vt import VirusTotal
from trust.utils.urls import cleanup_entities

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get actions')

    parser.add_argument(
        '-a',
        '--all',
        help='All databases',
        action='store_true',
    )
    parser.add_argument(
        '-sbl',
        '--safebrowsinglist',
        help='Google Safe Browsing database',
        action='store_true',
    )
    parser.add_argument(
        '-sc',
        '--sucuri',
        help='Sucuri database',
        action='store_true',
    )
    parser.add_argument(
        '-uv',
        '--urlvoid',
        help='UrlVoid database',
        action='store_true',
    )
    parser.add_argument(
        '-vt',
        '--virustotal',
        help='VirusTotal database',
        action='store_true',
    )
    parser.add_argument(
        '-i',
        '--input',
        help='Path to input file',
        action='store',
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Path to output file',
        action='store',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        help='Verbose output',
        action='store_true',
    )
    parser.add_argument(
        '-f',
        '--fast',
        help='Fast check',
        action='store_true',
    )

    arg = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    if arg.input is None:
        print('Path to input file not passed', file=sys.stderr)
        sys.exit(1)
    else:
        with open(arg.input, 'r') as f:
            entities = set([i.strip() for i in f.readlines()])
            cleaned_entities = cleanup_entities(entities)

        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            handlers=[
                logging.StreamHandler(),
                logging.FileHandler(arg.output)
            ],
            level=logging.DEBUG if arg.verbose else logging.INFO,
        )
        logger = logging.getLogger('main')

    if arg.all is True:
        arg.safebrowsinglist = True
        arg.fast = True

    if arg.fast is True:
        arg.sucuri = True
        arg.urlvoid = True
        arg.virustotal = True

    if arg.safebrowsinglist:
        sbl = SBL()
        sbl.check(cleaned_entities)

    if arg.sucuri:
        sc = Sucuri()
        sc.check(cleaned_entities)
    if arg.urlvoid:
        uv = UrlVoid()
        uv.check(cleaned_entities)
    if arg.virustotal:
        vt = VirusTotal()
        vt.check(entities)
