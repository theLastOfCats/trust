import logging
from urllib.parse import urlparse

from virus_total_apis import PublicApi as VirusTotalPublicApi

import config
from trust.utils.urls import is_url

logger = logging.getLogger('vt')


def get_categories(results: dict):
    categories = dict((k, v) for k, v in results.items()
                      if k.endswith('category'))
    return categories


class VirusTotal:
    def __init__(self) -> None:
        self.api_key = config.virustotal_api_key
        self.vt = VirusTotalPublicApi(self.api_key)

    def scan_url(self, url):
        response = self.vt.scan_url(url, timeout=30)

        while response.get('status_code') == 200:
            response = self.vt.scan_url(url, timeout=30)

    def check_url(self, entity):
        response = self.vt.get_url_report(entity, timeout=30)

        results = response.get('results')

        if results:
            logger.info(f'Entity: {entity}, Scan date: {results.get("scan_date")}')

            positives = f'{results.get("positives")}/{results.get("total")}'
            logger.info(f'Entity: {entity}, Total positives: {positives}')
        else:
            logger.info(f'Entity: {entity}. URL not found in VT database, try again later.')

    def check_domain(self, entity):
        response = self.vt.get_domain_report(entity, timeout=30)

        results = response.get('results')

        if results:
            for k, v in get_categories(results).items():
                logger.info(f'Entity: {entity}, {k}: {v}')

            whois = results.get('whois')
            if whois:
                for i in whois.split('\n'):
                    item = i.split(':')
                    key = item[0]
                    value = ':'.join(item[1:])

                    logger.info(f'Entity: {entity}, {key}: {value.strip()}')
        else:
            logger.info(f'Entity: {entity}. '
                        f'Domain not found in VT database, try again later.')

    def check(self, entities) -> None:
        for entity in entities:
            if is_url(entity):
                url = urlparse(entity)

                if url.scheme and url.netloc:
                    self.scan_url(entity)

                    self.check_url(entity)
                    self.check_domain(url.netloc)
                else:
                    for scheme in {'http://', 'https://'}:
                        url = scheme + entity

                        self.scan_url(url)
                        self.check_url(url)
                    self.check_domain(entity)
