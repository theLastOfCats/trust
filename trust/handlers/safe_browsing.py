import logging
import os
import tempfile

from gglsbl import SafeBrowsingList

import config
from trust.utils.urls import get_domains_list

logging.getLogger('gglsbl').setLevel(logging.ERROR)
logging.getLogger('googleapiclient').setLevel(logging.ERROR)
logger = logging.getLogger('sbl')


class SBL:
    def __init__(self) -> None:
        self.api_key = config.gcloud_safe_browse_api_key
        self.db_path = os.path.join(tempfile.gettempdir(), 'gsb_v4.db')
        self.sbl = SafeBrowsingList(api_key=self.api_key, db_path=self.db_path)
        self.sbl.update_hash_prefix_cache()

    def update_cache(self) -> None:
        self.sbl.update_hash_prefix_cache()

    def check(self, entities: set) -> None:
        for entity in get_domains_list(entities):
            threat_list = self.sbl.lookup_url(entity)
            if threat_list:
                logger.info(f'Entity: {entity}, Threat: {str(threat_list)}')
            else:
                logger.info(f'Entity: {entity}, Threat: no threat')
