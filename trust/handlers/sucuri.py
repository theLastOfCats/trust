import logging

import requests

from trust.utils.requests import headers
from trust.utils.urls import get_domains_list

logger = logging.getLogger('sucuri')


class Sucuri:
    def __init__(self):
        self.base_url = 'https://sitecheck.sucuri.net/api/v2/?json&scan={}'

    def check(self, entities):
        for entity in get_domains_list(entities):
            url = self.base_url.format(entity)
            response = requests.get(url, headers=headers)

            report = response.json()
            blacklist = report.get('BLACKLIST')
            warnings = blacklist.get('WARN')

            if warnings:
                for warn in warnings:
                    logger.info(f'Entity: {entity}, Status: {warn[0]}')
            else:
                logger.info(f'Entity: {entity}, Status: Domain clean')
