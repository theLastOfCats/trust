import logging
from datetime import datetime
from xml.etree.ElementTree import XML

import requests

import config
from trust.utils.urls import get_domains_list

logger = logging.getLogger('urlvoid')


class UrlVoid:
    def __init__(self) -> None:
        self.api_key = config.urlvoid_api_key
        self.base_url = 'http://api.urlvoid.com/api1000/{}/host/{}/scan'

    def check(self, entities: set) -> None:
        for entity in get_domains_list(entities):
            url = self.base_url.format(self.api_key, entity)
            response = requests.get(url)

            xml_tree = XML(response.text)
            parsed_domain_age = xml_tree.find('*domain_age')
            if parsed_domain_age is None or parsed_domain_age.text == '0':
                logger.info(f'Entity: {entity}, Status: Domain not found in database')
            else:
                domain_age = datetime.fromtimestamp(int(xml_tree.find('*domain_age').text))

                logger.info(f'Entity: {entity}, Domain Age: {domain_age}')
